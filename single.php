<?php get_header(); ?>
<article id="main-content" role="article">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 cell" id="post-<?php the_ID(); ?>">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'templates/content', 'single' ); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
	<?php the_content(); ?>
</article>
<?php get_footer(); ?>

<footer>
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell small-12 large-3">
                <ul class="social-media-menu">
                    <?php if( get_field( 'twitter_url', 'options' ) ) : ?>
                    <li class="social-media-menu__item">
                        <a href="<?php the_field( 'twitter_url', 'options' ); ?>" class="social-media-menu__item-link fa-stack fa-lg">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                            <span class="sr-only"><?php _e( 'Follow us on Twitter', 'hex-theme' ); ?></span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if( get_field( 'facebook_url', 'options' ) ) : ?>
                    <li class="social-media-menu__item">
                        <a href="<?php the_field( 'facebook_url', 'options' ); ?>" class="social-media-menu__item-link fa-stack fa-lg">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
                            <span class="sr-only"><?php _e( 'Follow us on Facebook', 'hex-theme' ); ?></span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if( get_field( 'instagram_url', 'options' ) ) : ?>
                    <li class="social-media-menu__item">
                        <a href="<?php the_field( 'instagram_url', 'options' ); ?>" class="social-media-menu__item-link fa-stack fa-lg">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                            <span class="sr-only"><?php _e( 'Follow us on Instagram', 'hex-theme' ); ?></span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if( get_field( 'linkedin_url', 'options' ) ) : ?>
                    <li class="social-media-menu__item">
                        <a href="<?php the_field( 'linkedin_url', 'options' ); ?>" class="social-media-menu__item-link fa-stack fa-lg">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-linkedin-in fa-stack-1x fa-inverse"></i>
                            <span class="sr-only"><?php _e( 'Connect with us on Linkedin', 'hex-theme' ); ?></span>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
            <?php if( is_active_sidebar( 'Footer Widgets' ) ) :
				 dynamic_sidebar( 'Footer Widgets' );
			endif; ?>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>


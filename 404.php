<?php get_header(); ?>
<section id="main-content" role="main">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 cell">
				<h1><?php _e( 'Sorry, your page was not found', 'hex-theme' ); ?></h1>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>

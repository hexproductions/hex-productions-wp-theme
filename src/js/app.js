// @codekit-prepend "../../node_modules/jquery/dist/jquery.js";
// @codekit-prepend "../../node_modules/foundation-sites/dist/js/foundation.min.js";

jQuery(function ($) {

	// Call the pretty Foundation scripts
	$(document).foundation();

	// Make the entire div containing link clickable.
	$(".link-box").click(function(){
		if($(this).find("a").length){
	       window.location.href = $(this).find("a:first").attr("href");
	       var $a = $(this).find("a:first");
	       window.open($a.attr("href"), $a.attr("target") || '_self');
	       return false;
		}
	});

	// Insert screen reader text for pagination on archive pages.
	$(".page-numbers a:not(.next)").each(function() {
		$(this).prepend('<span class="sr-only">Page number </span>');
	});

	// Check external links and add class.
	function link_is_external(link_element) {
		if( link_element.href && !link_element.href.includes('mailto:') && !link_element.href.includes('tel:') ) {
			return (link_element.host !== window.location.host);
		}
	}

	$('a').each(function() {
		if (link_is_external(this) && !$(this).hasClass('social-media-menu__item-link') && !$(this).hasClass('social-icons__list-link') ) {
			$(this).addClass('external');
		}
	})
	
	// Detect PDFs in links and add attributes.
    function link_is_document(link_document) {
        if (link_document.href.includes('.pdf')) {
            return true;
        }
    }

	$('a').each(function() {
        if (link_is_document(this)) {
            $(this).addClass('pdf');
            $(this).attr('aria-label', 'This is a link to a PDF Document');
        }
    })

	// Add aria-labels to required form fields
	$('.frm_required').each(function() {
		$(this).parent().attr('aria-label', 'This is a required field');
		$(this).text('* (required)');
	})

	/*****
	 * High Contrast Cookie
	*******/

	// Read cookie
	function getCookie(strCookie){
		var strName = strCookie + "=";
		var arrCookies = document.cookie.split(';');
		for (var i = 0; i < arrCookies.length; i++) {
			var strValorCookie = arrCookies[i];
			while (strValorCookie.charAt(0) == ' ') {
				strValorCookie = strValorCookie.substring(1, strValorCookie.length);
			}
			if (strValorCookie.indexOf(strName) == 0) {
				return strValorCookie.substring(strName.length, strValorCookie.length);
			}
		}
		return '';
	}

	// Set cookie
	function setCookie(name, value, expires, path, domain, secure) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );

	/* if the expires variable is set, make the correct expires time, the current script below will set
	it for x number of days, to make it for hours, delete * 24, for minutes, delete * 60 * 24 */
	path = '/';
	if ( expires ) {
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );
	document.cookie = name + "=" +escape( value ) + 
	( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + 
	( ( path ) ? ";path=" + path : "" ) + 
	( ( domain ) ? ";domain=" + domain : "" ) + 
	( ( secure ) ? ";secure" : "" );
	}

	// Erase cookie
	function eraseCookie(name) {
		setCookie(name, '', -1);
	}

	$(document).ready(function() {

		var cookieContrast = getCookie("highContrast");

		if (cookieContrast == "highContrast"){
			$("body").addClass("highContrast");
		}

		$("a.highContrast").click(function(){
			var x = getCookie("highContrast");
			if (x == "highContrast") {
				$("body").removeClass("highContrast");
				eraseCookie("highContrast");
			} else {
				$("body").addClass("highContrast");
				setCookie("highContrast", "highContrast");
			}
			return (false);
		});
	});

	/*****
	 * Alert Banner
	*******/

	// Get current time.
	var currentTime = new Date().getTime();

	// Add hours function.
	Date.prototype.addHours = function(h) {    
		this.setTime( this.getTime() + ( h*60*60*1000) ); 
		return this;   
	}

	// Get time after 24 hours.
	var after24 = new Date().addHours(10).getTime();

	// Hide alert banner on click.
	$( '.site-alert__close' ).click( function() {
		$(this).closest( '.site-alert' ).fadeOut();
		localStorage.setItem( 'desiredTime', after24 ); 
	});

	// Show / Hide alert base on time.
	if( localStorage.getItem( 'desiredTime' ) >= currentTime ) {
		$( '.site-alert' ).addClass( 'hide' );
	} else {
		$( '.site-alert' ).removeClass( 'hide' );
	}
});

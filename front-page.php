<?php get_header(); ?>
<section id="main-content" role="main">
	<?php get_template_part( 'templates/content-hero' ); ?>
	<?php the_content(); ?>
</section>
<?php get_footer(); ?>

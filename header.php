<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php if ( is_search() ) { ?>
		<meta name="robots" content="noindex, nofollow" />
	<?php } ?>
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<link rel="shortcut icon" href="<?php echo esc_url( home_url( '/' ) ); ?>favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/mstile-310x310.png" />
	<link rel="dns-prefetch" href="//www.google-analytics.com">
	<?php wp_head(); ?>
</head>
<body <?php body_class( 'no-js' ); ?>>
<a href="#main-content" id="skiptocontent"><?php _e( 'Skip to main content', 'hex-theme' ); ?></a>
<header>
	<div class="top-banner">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<?php if( get_field( 'phone_number', 'options' ) ) : ?>
				<div class="cell small-12 large-2 tel-number">
					<i class="fas fa-phone"></i>
					<?php echo format_phone( get_field( 'phone_number', 'options' ) ); ?>
				</div>
				<?php endif; ?>
				<?php if( get_field( 'email_address', 'options' ) ) : ?>
				<div class="cell small-12 large-4 email-address">
					<i class="fas fa-envelope"></i>
					<a href="mailto:<?php the_field( 'email_address', 'options' ); ?>">
						<?php the_field( 'email_address', 'options' ); ?>
					</a>
				</div>
				<?php endif; ?>
				<div class="cell small-12 large-6">
					<ul class="a11y-menu">
						<li class="a11y-menu__item">
							<i class="fas fa-adjust"></i>
							<a href="#" class="highContrast" title="High contrast"><?php _e( 'Contrast', 'hex-theme' ); ?></a>
						</li>
						<li class="a11y-menu__item">
							<i class="fas fa-universal-access"></i>
							<a href="<?php echo get_site_url() . '/accessibility'; ?>"><?php _e( 'Accessibility', 'hex-theme' ); ?></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="menu-wrapper">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell small-6 medium-2 site-logo">
					<a href="<?php echo get_site_url(); ?>">
						<img src="<?php echo get_template_directory_uri() . '/assets/img/HeX-Productions-Logo.png'; ?>" alt="">
						<span class="sr-only"><?php _e( 'Return to home page', 'hex-theme' ); ?></span>
					</a>
				</div>
				<div class="cell small-6 hide-for-medium">
					<div class="title-bar" data-responsive-toggle="the-menu" data-hide-for="medium">
						<button class="menu-icon" type="button" data-toggle>
							<span class="sr-only"><?php _e( 'Open main menu', 'hex-theme' ); ?></span>
							<span class="title-bar__title"><?php _e( 'Menu', 'hex-theme' ); ?></span>
						</button>
					</div>
				</div>
				<div class="cell small-12 medium-10">
					<nav class="navbar" id="the-menu" aria-label="Main">
					<?php
					wp_nav_menu( array(
						'container'      => false,
						'menu'           => 'main_menu',
						'theme_location' => 'Header',
						'items_wrap'     => '<ul class="navbar-menu show-for-medium">%3$s</ul>',
					) );

					wp_nav_menu( array(
						'container'      => false,
						'menu'           => 'main_menu',
						'theme_location' => 'Header',
						'items_wrap'     => '<ul class="vertical menu drilldown show-for-small-only" data-drilldown data-auto-height="true" data-animate-height="true" data-parent-link="true">%3$s</ul>',
					) );
					?>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>
<?php get_template_part( 'templates/content', 'alert' ); ?>

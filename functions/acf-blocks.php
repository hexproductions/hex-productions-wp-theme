<?php

function register_acf_block_types() {

    acf_register_block_type(array(
        'name'              => 'accordion',
        'title'             => __( 'Accordion' ),
        'description'       => __( 'A block that allows you to build an accordion.' ),
        'render_template'   => 'blocks/accordion.php',
        'category'          => 'custom-blocks',
        'icon'              => 'list-view',
        'keywords'          => array( 'accordion' ),
    ));

    acf_register_block_type(array(
        'name'              => 'card-repeater',
        'title'             => __( 'Card repeater' ),
        'description'       => __( 'A block that displays up to three cards.' ),
        'render_template'   => 'blocks/card-repeater.php',
        'category'          => 'custom-blocks',
        'icon'              => 'images-alt2',
        'keywords'          => array( 'card', 'repeater' ),
    ));

    acf_register_block_type(array(
        'name'              => 'graph',
        'title'             => __( 'Graph' ),
        'description'       => __( 'A block that displays a graph.' ),
        'render_template'   => 'blocks/graph.php',
        'category'          => 'custom-blocks',
        'icon'              => 'chart-bar',
        'keywords'          => array( 'graph' ),
    ));

    acf_register_block_type(array(
        'name'              => 'latest-news',
        'title'             => __( 'Latest news' ),
        'description'       => __( 'A block that displays a page or post.' ),
        'render_template'   => 'blocks/latest-news.php',
        'category'          => 'custom-blocks',
        'icon'              => 'analytics',
        'keywords'          => array( 'latest', 'news' ),
    ));

    acf_register_block_type(array(
        'name'              => 'logo-banner',
        'title'             => __( 'Logo banner' ),
        'description'       => __( 'A block that displays a banner of logos.' ),
        'render_template'   => 'blocks/logo-banner.php',
        'category'          => 'custom-blocks',
        'icon'              => 'editor-kitchensink',
        'keywords'          => array( 'logo', 'banner' ),
    ));

    acf_register_block_type(array(
        'name'              => 'video',
        'title'             => __( 'Video' ),
        'description'       => __( 'A block that displays an embedded video' ),
        'render_template'   => 'blocks/video.php',
        'category'          => 'custom-blocks',
        'icon'              => 'media-video',
        'keywords'          => array( 'video', 'embed' ),
    ));
}

if( function_exists( 'acf_register_block_type' ) ) {
    add_action( 'acf/init', 'register_acf_block_types' );
}

// Only load Chart JS when block is used on a page.
add_action( 'wp_enqueue_scripts', 'register_acf_block_styles' );
add_action( 'admin_enqueue_scripts', 'register_acf_block_styles' );

function register_acf_block_styles() : void {
  if( has_block( 'acf/graph' ) ) {
    wp_enqueue_script( 'graph', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js', array(), '2.9.4', false );
  }
}

// Creates category to put all the ACF blocks in.
function hex_custom_blocks( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'custom-blocks',
				'title' => __( 'Hex Custom Blocks', 'custom-blocks' ),
			),
		)
	);
}
add_filter( 'block_categories_all', 'hex_custom_blocks', 10, 2);

?>

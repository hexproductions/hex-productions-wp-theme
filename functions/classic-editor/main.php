<?php

// TODO: Needs refactoring at later date.

@require_once __DIR__ . '/vendor/autoload.php';

if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}

function sanitise_post_data($post_id)
{
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    };

    $config = json_decode(file_get_contents(sprintf('%s/html_cleaner.config.json', get_template_directory())));

    $post = get_post($post_id);

    write_log(sprintf('Saving post #%1$s...', strval($post->ID)));
    write_log('...');
    write_log('...');

    write_log(sprintf('%1$s BEFORE %1$s', str_repeat('-', 40)));
    write_log($post->post_content);

    if ($post->post_content !== strip_tags($post->post_content)) {
        $blocks = parse_blocks($post->post_content);
        $replacements = array();
        foreach ($blocks as $block) {
            switch (strval($block['blockName'])) {
                case '':
                case 'core/freeform':
                    $replacements[strval($block['innerHTML'])] = sanitise_html(strval($block['innerHTML']), $config);
                    break;
            }
        }

        $post->post_content = strtr($post->post_content, $replacements);

        remove_action('save_post', 'sanitise_post_data');

        wp_update_post(array('ID' => $post->ID, 'post_content' => $post->post_content));

        add_action('save_post', 'sanitise_post_data');
    }

    write_log(sprintf('%1$s AFTER %1$s', str_repeat('-', 40)));
    write_log($post->post_content);

    write_log(str_repeat('=', 90));
}

function sanitise_html($html, $config)
{
    if (strlen(trim(strval($html))) < 1) return '';

    $document = new DOMWrap\Document();
    $document->html($html);
    $nodes = $document->find('body *')->each(function ($node) use ($config) {
        /**
         * @var \DOMWrap\Element $node
         */
        //write_log($node);

        $whitelist = (isset($config->whitelist) && is_array($config->whitelist)) ? $config->whitelist : array();
        $blacklist = (isset($config->blacklist) && is_array($config->blacklist)) ? $config->blacklist : array();

        $keepElement = true;
        $keepAttribute = array();

        foreach ($blacklist as $rule) {
            $rule = explode(':', $rule, 2);
            $elements = (isset($rule[0])) ? array_filter(explode(',', strval($rule[0]))) : array();
            $attributes = (isset($rule[1])) ? array_filter(explode(',', strval($rule[1]))) : null;

            foreach ($elements as $element) {
                if (fnmatch(trim(strval($element)), trim(strval($node->tagName)), FNM_CASEFOLD)) {
                    if (is_array($attributes)) {
                        foreach ($attributes as $attribute) {
                            foreach ($node->attributes as $index => $nodeAttribute) {
                                if (fnmatch(trim(strval($attribute)), trim(strval($nodeAttribute->name)), FNM_CASEFOLD)) {
                                    $keepAttribute[$nodeAttribute->name] = false;
                                }
                            }
                        }
                    } else {
                        $keepElement = false;
                    }
                }
            }
        }

        foreach ($whitelist as $rule) {
            $rule = explode(':', $rule, 2);
            $elements = (isset($rule[0])) ? array_filter(explode(',', strval($rule[0]))) : array();
            $attributes = (isset($rule[1])) ? array_filter(explode(',', strval($rule[1]))) : null;

            foreach ($elements as $element) {
                if (fnmatch(trim(strval($element)), trim(strval($node->tagName)), FNM_CASEFOLD)) {
                    if (is_array($attributes)) {
                        foreach ($attributes as $attribute) {
                            foreach ($node->attributes as $index => $nodeAttribute) {
                                if (fnmatch(trim(strval($attribute)), trim(strval($nodeAttribute->name)), FNM_CASEFOLD)) {
                                    $keepAttribute[$nodeAttribute->name] = true;
                                    $keepElement = true;
                                }
                            }
                        }
                    } else {
                        $keepElement = true;
                    }
                }
            }
        }

        //write_log('keepElement: ' . $keepElement);
        //write_log('keepAttribute: ' . json_encode($keepAttribute, JSON_PRETTY_PRINT));

        if (!$keepElement) {
            $node->destroy();
        } else {
            foreach ($keepAttribute as $attributeName => $shouldKeep) {
                if (!$shouldKeep) {
                    $node->removeAttr($attributeName);
                }
            }
        }
    });
    return (new \DOMWrap\Document())->html($document->html())->find('body')->html();
}

//add_action('save_post', 'sanitise_post_data');

<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-develop',
    'version' => 'dev-develop',
    'aliases' => 
    array (
    ),
    'reference' => '9a112b5952e53e980ff13fac03816a16ca2a797f',
    'name' => 'hex-productions/classic-editor',
  ),
  'versions' => 
  array (
    'hex-productions/classic-editor' => 
    array (
      'pretty_version' => 'dev-develop',
      'version' => 'dev-develop',
      'aliases' => 
      array (
      ),
      'reference' => '9a112b5952e53e980ff13fac03816a16ca2a797f',
    ),
    'scotteh/php-dom-wrapper' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '120226c96ab6143167fe1d6964d97a7d7768e46c',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fcd0b29a7a0b1bb5bfbedc6231583d77fea04814',
    ),
  ),
);

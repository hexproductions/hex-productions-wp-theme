<?php

/**
 *  Custom Post Type - Our work
 */

add_action( 'init', 'register_cpt_ourwork' );

function register_cpt_ourwork() {
    $labels = array(
        'name' => _x( 'Our Work', 'ourwork' ),
        'singular_name' => _x( 'Work item', 'ourwork' ),
        'add_new' => _x( 'Add New Work item', 'ourwork' ),
        'add_new_item' => _x( 'Add New Work item', 'ourwork' ),
        'edit_item' => _x( 'Edit Work items', 'ourwork' ),
        'new_item' => _x( 'New Work item', 'ourwork' ),
        'view_item' => _x( 'View Work items', 'ourwork' ),
        'search_items' => _x( 'Search Work items', 'ourwork' ),
        'not_found' => _x( 'No Work items found', 'ourwork' ),
        'not_found_in_trash' => _x( 'No Work found in Trash', 'ourwork' ),
        'parent_item_colon' => _x( 'Parent Work item:', 'ourwork' ),
        'menu_name' => _x( 'Our Work', 'ourwork' )
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Add different ourwork items to the website to be displayed',
        'supports' => array( 'title', 'excerpt', 'revisions', 'editor', 'thumbnail' ),
        'taxonomies'  => array( 'category' ),
        'public' => true,
        'show_in_rest' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array( 'slug' => 'our-work', 'with_front' => false ),
        'capability_type' => 'post',
	    'menu_icon' => 'dashicons-media-document');

    register_post_type( 'ourwork', $args );
}

?>

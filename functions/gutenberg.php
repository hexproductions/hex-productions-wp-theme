<?php

/**
 * 	Allows Guteberg blocks specified in the array.
 */
add_filter( 'allowed_block_types_all', 'hex_allowed_block_types' );

function hex_allowed_block_types( $allowed_blocks ) {
	return array(
		// Allowed Gutenberg core blocks.
		'core/freeform',
		'core/heading',
		'core/image',
		'core/paragraph',
		'core/list',
		'core/list-item',
		'core/block',
		'core/shortcode',

		// Allowed ACF blocks should added here. All blocks registered at acf/block name.
		'acf/accordion',
		'acf/card-repeater',
		'acf/graph',
		'acf/latest-news',
		'acf/logo-banner',
		'acf/video',
	);
}

/**
 * 	Load editor styles for the ACF Gutenberg blocks.
 */

add_theme_support( 'editor-styles' );
add_editor_style( '/assets/css/main.css' );

/**
 * 	Remove Core Gutenberg Block Patters from block sidebar.
 */

remove_theme_support( 'core-block-patterns' );

/**
 * 	
 *  Remove Gutenberg widgets editor.
 */

function remove_block_widgets() {
    remove_theme_support( 'widgets-block-editor' );
}

add_action( 'after_setup_theme', 'remove_block_widgets' );

/**
 * 	Remove Gutenberg Custom Appearance Setting from block sidebar.
 */

function disable_gutenberg_custom_settings() {
	add_theme_support( 'editor-font-sizes' );
	add_theme_support( 'editor-color-palette' );
	add_theme_support( 'editor-gradient-presets' );
	add_theme_support( 'disable-custom-font-sizes' );
	add_theme_support( 'disable-custom-colors' );
	add_theme_support( 'disable-custom-gradients' );
}

add_action( 'after_setup_theme', 'disable_gutenberg_custom_settings' );

function disable_drop_cap($editor_settings) {
    $editor_settings['__experimentalFeatures']['typography']['dropCap'] = false;
    return $editor_settings;
}

add_filter('block_editor_settings_all', 'disable_drop_cap');

// Remove Gutenberg-related stylesheets.
add_action( 'wp_enqueue_scripts', 'remove_block_css', 100 );

function remove_block_css() {
	wp_dequeue_style( 'wp-block-library' ); // Wordpress core
	wp_dequeue_style( 'wp-block-library-theme' ); // Wordpress core
	wp_dequeue_style( 'wc-block-style' ); // WooCommerce
}

/**
 * Applies wrapping markup around core Gutenberg blocks.
 *
 * @param  string $block_content  The block content about to be appended.
 * @param  array  $block          The full block, including name and attributes.
 */

function hex_wrap_core_blocks( $block_content, $block ) {

	if ( ( $block['blockName'] === 'core/paragraph' ) ) {
		$block_content = '<div class="paragraph-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . $block_content . '</div></div></div></div>';
	}
	if ( ( $block['blockName'] === 'core/quote' ) ) {
		$block_content = '<div class="quote-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . $block_content . '</div></div></div></div>';
	}
	if ( ( $block['blockName'] === 'core/columns' ) ) {
		$block_content = '<div class="columns-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . $block_content . '</div></div></div></div>';
	}
	if ( ( $block['blockName'] === 'core/list' ) ) {
		$block_content = '<div class="list-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . $block_content . '</div></div></div></div>';
	}
	if ( ( $block['blockName'] === 'core/heading' ) ) {
		$block_content = '<div class="heading-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . $block_content . '</div></div></div></div>';
	}
	if ( ( $block['blockName'] === 'core/image' ) ) {
		$block_content = '<div class="image-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . $block_content . '</div></div></div></div>';
	}
	if ( ( $block['blockName'] === 'core/shortcode' ) ) {
		$block_content = '<div class="shortcode-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . $block_content . '</div></div></div></div>';
	}
	if ( ( $block['blockName'] === 'core/embed' ) || ( $block['blockName'] === 'core-embed/youtube' ) || ( $block['blockName'] === 'core-embed/vimeo' ) || ( $block['blockName'] === 'core-embed/soundcloud' ) || ( $block['blockName'] === 'core-embed/spotify' ) ) {
		$block_content = '<div class="embed-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . $block_content . '</div></div></div></div>';
	}
	if ( null === $block['blockName'] && ! empty( $block_content ) && ! ctype_space( $block_content ) ) {
		$block_content = '<div class="classic-editor-block"><div class="grid-container"><div class="grid-x grid-padding-x"><div class="small-12 cell">' . wpautop($block_content) . '</div></div></div></div>';
	}
	return $block_content;
}

add_filter( 'render_block', 'hex_wrap_core_blocks', 10, 2 );
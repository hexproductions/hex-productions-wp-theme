<?php

add_filter( 'the_content', 'remove_title_attr' );

function remove_title_attr($text) {

    // Get all title="..." tags from the html.
    $result = array();
    preg_match_all('|title="[^"]*"|U', $text, $result);

    // Replace all occurances with an empty string.
    foreach($result[0] as $html_tag) {
        $text = str_replace($html_tag, '', $text);
    }

    return $text;
}

add_filter( 'nav_menu_link_attributes', 'add_aria_haspopup_atts', 10, 3 );

function add_aria_haspopup_atts( $atts, $item, $args ) {
  if ( in_array( 'menu-item-has-children', $item->classes )) {
    $atts['aria-haspopup'] = 'true';
  }
  return $atts;
}

?>

<?php

/**
 * 	Sitemap shortcode
 */

function sitemap() {
    $sitemap = '<div class="sitemap">';
    $sitemap .= '<h2>Pages</h2>';
	$sitemap .= '<ul>';
	$sitemap .= 
	wp_list_pages( array( 
	  'exclude' => '',
	  'title_li' => '',
	  'echo' => false,
	) );
	$sitemap .= '</ul></div>';
    return $sitemap;
}

add_shortcode( 'sitemap', 'sitemap' );

?>

<?php

// TODO: Needs refactoring later...

@require_once __DIR__ . '/../classic-editor/vendor/autoload.php';

if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}

add_filter('the_content', 'fix_youtube_video_titles', PHP_INT_MAX);

function fix_youtube_video_titles($content)
{
    if ($content !== strip_tags($content)) {
        $document = new DOMWrap\Document();
        $document->html($content);
        $nodes = $document->find('body iframe')->each(function ($node) {
            /**
             * @var \DOMWrap\Element $node
             */
            if ($node->hasAttr('src') && !$node->hasAttr('title')) {
                $src = $node->attr('src');
                write_log($src);
                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $src, $match);
                if (isset($match[1])) {
                    $youtube_id = $match[1];
                    write_log('YouTube video ID: ' . $youtube_id);

                    $api_key = "AIzaSyCCndUFzjUvSf-YxiOU-7ZkSvM5cwBlhqI"; // Google API key for YouTube
                    $url = sprintf("https://www.googleapis.com/youtube/v3/videos?id=%s&key=%s&part=snippet,contentDetails,statistics,status", $youtube_id, $api_key);
                    $json = file_get_contents($url);
                    $data = json_decode($json, true);
                    $title = null;
                    foreach ((array)$data['items'] as $key => $gDat) {
                        $title = $gDat['snippet']['title'] ?? $title;
                    }
                    if (strlen(trim(strval($title))) > 0) {
                        write_log('Video title retrieved: "' . trim(strval($title)) . '"');
                        $node->attr('title', trim(strval($title)));
                    }
                }
            }
        });
        return (new \DOMWrap\Document())->html($document->html())->find('body')->html();
    }

    return $content;
}

add_filter('the_content', 'fix_vimeo_video_titles', PHP_INT_MAX);

function fix_vimeo_video_titles($content)
{
    if ($content !== strip_tags($content)) {
        $document = new DOMWrap\Document();
        $document->html($content);
        $nodes = $document->find('body iframe')->each(function ($node) {
            /**
             * @var \DOMWrap\Element $node
             */
            if ($node->hasAttr('src') && !$node->hasAttr('title')) {
                $src = $node->attr('src');
                write_log($src);
                preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\#?)(?:[?]?.*)$%im', $src, $match);
                if (isset($match[3])) {
                    $vimeo_id = $match[3];
                    write_log('Vimeo video ID: ' . $vimeo_id);

                    $url = sprintf("https://vimeo.com/api/oembed.json?url=%s", rawurlencode(sprintf('https://vimeo.com/%s', $vimeo_id)));
                    $json = file_get_contents($url);
                    $data = json_decode($json);
                    $title = $data->title ?? '';
                    if (strlen(trim(strval($title))) > 0) {
                        write_log('Video title retrieved: "' . trim(strval($title)) . '"');
                        $node->attr('title', trim(strval($title)));
                    }
                }
            }
        });
        return (new \DOMWrap\Document())->html($document->html())->find('body')->html();
    }

    return $content;
}

<?php

/**
 * 	Apply HeX Logo on Admin Screen
 */

function hex_logo_login() {
?>
<style type="text/css">
body.login div#login h1 a {
 	background-image: url(https://www.horlix.com/wp-content/themes/hex-productions/assets/img/HeX-Productions-Logo.png);
 	padding: 5rem 0 0 0;
	background-size: 100%;
	background-repeat: no-repeat;
	height: auto;
	width: auto;
}
</style>
 <?php
} add_action( 'login_enqueue_scripts', 'hex_logo_login' );

/**
 * 	Apply HeX RSS feed to admin widgets
 */

function widget_latest_hex() {
	wp_add_dashboard_widget('widget_freelanceswitch', __('Latest from HeX Productions', 'rc_mdm'), 'widget_latest_hex_rss');
}
add_action('wp_dashboard_setup', 'widget_latest_hex');


function widget_latest_hex_rss() {

	// My feeds list (add your own RSS feeds urls)
	$my_feeds = array(
				'https://www.horlix.com/feed/',
				'https://www.eventbrite.co.uk/rss/organizer_list_events/17313230160'
				);

        echo '<a href="http://www.horlix.com" target="_blank" aria-label="Visit the HeX Productions Website"><img src="https://www.horlix.com/wp-content/themes/hex-productions/assets/img/HeX-Productions-Logo.png" style="width: 100%;" /></a>';
	// Loop through Feeds
	foreach ( $my_feeds as $feed) :

		// Get a SimplePie feed object from the specified feed source.
		$rss = fetch_feed( $feed );
		if (!is_wp_error( $rss ) ) : // Checks that the object is created correctly
		    // Figure out how many total items there are, and choose a limit
		    $maxitems = $rss->get_item_quantity( 3 );

		    // Build an array of all the items, starting with element 0 (first element).
		    $rss_items = $rss->get_items( 0, $maxitems );

		    // Get RSS title
		    $rss_title = '<a href="'.$rss->get_permalink().'" target="_blank">'.strtoupper( $rss->get_title() ).'</a>';
		endif;

		echo '<div class="widget-area" style="padding: 1rem 0 0 0; border-bottom: solid 1px #CCC">';

		// Check items
		if ( $maxitems == 0 ) {
			echo '<div>'.__( 'No item', 'rc_mdm').'.</div>';
		} else {
			// Loop through each feed item and display each item as a hyperlink.
			foreach ( $rss_items as $item ) :
				// Uncomment line below to display non human date
				//$item_date = $item->get_date( get_option('date_format').' @ '.get_option('time_format') );

				// Get human date (comment if you want to use non human date)
				$item_date = human_time_diff( $item->get_date('U'), current_time('timestamp')).' '.__( 'ago', 'rc_mdm' );
        echo '<div class="widget-item" style="margin-bottom: 1rem;">';
				echo '<h4><strong><a href="'.esc_url( $item->get_permalink() ).'">' . esc_html( $item->get_title() ) . '</a></strong></h4>';
        echo '<div style="padding: 0 0 1rem; 0">';
				$content = $item->get_content();
        echo '</div>';
				// Shorten content
				$content = wp_html_excerpt($content, 250) . ' [...]';

				echo $content;

				echo '</div>';
			endforeach;
		}

		echo '</div>';

	endforeach; // End foreach feed
  ?>

  <p>
  <a href="https://www.horlix.com" target="_blank">HeX Productions <span class="screen-reader-text">(opens in a new tab)</span><span aria-hidden="true" class="dashicons dashicons-external"></span></a>
  |
  <a href="https://www.accessibilitynottingham.co.uk" target="_blank">Accessibility Nottingham <span class="screen-reader-text">(opens in a new tab)</span><span aria-hidden="true" class="dashicons dashicons-external"></span></a>
</p>
  <?php
}

?>

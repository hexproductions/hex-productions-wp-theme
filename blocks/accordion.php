<?php

/**
 * Accordion Block Template.
 **/

if( have_rows( 'hex_accordion_items' ) ) :

?>
<section class="accordion-block block-margin">
  <?php if( get_field( 'hex_accordion_heading' ) ) : ?>
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="small-12 cell">
        <h2 class="accordion-block__heading"><?php the_field( 'hex_accordion_heading' ); ?></h2>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="small-12 cell">
        <ul class="accordion" data-accordion>
          <?php while ( have_rows( 'hex_accordion_items' ) ) : the_row(); ?>
            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title" aria-ignore="true"><?php the_sub_field( 'hex_accordion_title' ); ?></a>
              <div class="accordion-content" data-tab-content>
                <h3 class="sr-only"><?php the_sub_field( 'hex_accordion_title' ); ?></h3>
                <?php the_sub_field( 'hex_accordion_content' ); ?>
              </div>
            </li>
            <?php endwhile; ?>
        </ul>
      <?php endif; ?>
      </div>
    </div>
  </div>
</section>

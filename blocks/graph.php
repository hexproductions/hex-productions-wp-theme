<?php

/**
 * Graph Block Template.
 **/

//  Put the ACF Repeater data into three arrays.
$graph_labels  = array();
$graph_data    = array();
$graph_colours = array();

if( have_rows( 'graph_data' ) ) :
    while( have_rows( 'graph_data' ) ) : the_row();
        array_push( $graph_labels, get_sub_field( 'bar_label' ) );
        array_push( $graph_data, get_sub_field( 'bar_data' ) );
        array_push( $graph_colours, get_sub_field( 'bar_colour' ) );
    endwhile;
endif;

// Put the ACF repeater values in an associative array.
$aria_data = array_combine( $graph_labels, $graph_data );

// Loop over values to implode for the aria-label on the canvas element.
$aria_label = array();
foreach ( $aria_data as $key => $value ) {
  $aria_label[] = "$key: $value";
}
?>
<div class="graph-block block-margin">
    <div class="grid-container">
        <div class="grid-x grid-x-padding">
            <div class="small-12 cell">
                <canvas id="graphBlock-<?php echo $block['id']; ?>" aria-label="<?php echo get_field( 'graph_label' ) . '. ' . 'The values are: '.implode( ', ', $aria_label ); ?>" role="img"></canvas>
                <script>
                var ctx = document.getElementById( 'graphBlock-<?php echo $block['id']; ?>' );

                Chart.defaults.global.defaultFontFamily = 'Helvetica Neue';
                Chart.defaults.global.defaultFontColor  = '#000000';
                Chart.defaults.global.defaultFontSize   = 16;
                Chart.defaults.global.defaultColor      = 'rgba(62, 90, 128)';

                var myChart = new Chart(ctx, {
                    type: '<?php the_field( 'graph_type' ); ?>',
                    data: {
                        labels: <?php echo json_encode( array_values( $graph_labels ) ); ?>,
                        datasets: [{
                            label: '<?php the_field( 'graph_label' ); ?>',
                            data: <?php echo json_encode( array_values( $graph_data ) ); ?>,
                            <?php if( get_field( 'graph_type' ) !== 'line' ) : ?>
                            backgroundColor: <?php echo json_encode( array_values( $graph_colours ) ); ?>,
                            <?php else : ?>
                            backgroundColor: 'rgba(62, 90, 128)',
                            <?php endif; ?>
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        title: {
                            display: true,
                            text: '<?php the_field( 'graph_label' ); ?>'
                        },
                        <?php if( get_field( 'graph_type' ) !== 'pie' && get_field( 'graph_type' ) !== 'doughnut' ) : ?>
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                        <?php endif; ?>
                    }
                });
                </script>
            </div>
        </div>
    </div>
</div>

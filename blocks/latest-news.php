<?php

/**
 * Latest News Block Template.
 **/

?>
<?php if( get_field( 'hc_article' ) ) : ?>
<?php $featured_article_id = get_field( 'hc_article' );  ?>
    <div class="latest-news block-margin">
        <div class="grid-x">
            <div class="cell small-12 medium-6">
                <?php echo get_the_post_thumbnail( $featured_article_id, 'landscape', 'class=latest-news__image' ); ?>
            </div>
            <div class="cell small-12 medium-6">
                <div class="latest-news__card">
                <?php if( get_field( 'hc_intro_text' ) ) : ?>
                    <h2 class="latest-news__card-intro"><?php the_field( 'hc_intro_text' ); ?></h2>
                <?php endif; ?>
                <h3 class="latest-news__card-title h2"><?php echo get_the_title( $featured_article_id ); ?></h3>
                <?php if( has_excerpt( $featured_article_id ) ) : ?>
                    <p class="latest-news__card-excerpt"><?php echo get_the_excerpt( $featured_article_id ); ?></p>
                <?php endif; ?>
                    <a href="<?php echo get_the_permalink( $featured_article_id ); ?>" class="button large" aria-label="<?php echo 'Read ' . get_the_title( $featured_article_id ) . ' article'; ?>"><?php _e( 'Read article', 'homefield' ); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
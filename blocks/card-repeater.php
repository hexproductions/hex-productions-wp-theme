<?php

/**
 * Card Repeater Block Template.
 **/

?>
<?php if( have_rows( 'hc_cards' ) ) : ?>
    <div class="cards-block block-margin">
        <div class="grid-container" data-equalizer data-equalizer-on="medium">
            <div class="grid-x grid-padding-x">
            <?php while( have_rows( 'hc_cards' ) ) : the_row(); ?>
                <div class="cell small-12 medium-6 large-4">
                    <?php if( get_sub_field( 'page_link_or_free_text' ) == 'page-link' ) : ?>
                    <?php $hc_article_id = get_sub_field( 'hc_card' ); ?>
                    <div class="cards-block__card <?php if( get_sub_field( 'display_link' ) ) { echo'link-box'; }; ?>">
                        <?php if( has_post_thumbnail( $hc_article_id ) ) : ?>
                        <img src="<?php echo get_the_post_thumbnail_url( $hc_article_id, 'small-landscape' ); ?>" alt="" class="cards-block__card-image">
                        <?php else : ?>
                        <img src="<?php echo get_template_directory_uri() . '/assets/img/card-placeholder.jpg'; ?>" alt="" class="cards-block__card-image">
                        <?php endif; ?>
                        <div class="cards-block__card-content" data-equalizer-watch>
                            <?php if( !get_sub_field( 'display_link' ) ) : ?>
                            <h3 class="cards-block__card-title"><?php echo get_the_title( $hc_article_id ); ?></h3>
                            <?php else : ?>
                            <h2 class="cards-block__card-title h3"><?php echo get_the_title( $hc_article_id ); ?></h2>
                            <?php endif; ?>
                            <?php if( has_excerpt( $hc_article_id ) ) : ?>
                            <p class="cards-block__card-excerpt"><?php echo get_the_excerpt( $hc_article_id  ); ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="cards-block__card-link text-center">
                            <?php if( get_sub_field( 'display_link' ) ) : ?>
                            <a href="<?php echo get_permalink( $hc_article_id ); ?>" aria-label="Read more about <?php echo get_the_title( $hc_article_id ); ?>"><?php _e( 'Read more', 'homefield' ); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php elseif( get_sub_field( 'page_link_or_free_text' ) == 'free-text' ) : ?>
                        <div class="cards-block__card <?php if( get_sub_field( 'hc_card_link' ) ) { echo 'link-box'; }; ?>">
                            <?php if( get_sub_field( 'hc_card_image' ) ) : ?>
                            <?php echo wp_get_attachment_image( get_sub_field( 'hc_card_image' ), 'small-landscape', false, 'class=cards-block__card-image' ); ?>
                            <?php else : ?>
                            <img src="<?php echo get_template_directory_uri() . '/assets/img/card-placeholder.jpg'; ?>" alt="" class="cards-block__card-image">
                            <?php endif; ?>
                            <div class="cards-block__card-content" data-equalizer-watch>
                            <?php if( get_sub_field( 'hc_card_title' ) && get_sub_field( 'hc_card_link' ) ) : ?>
                            <h2 class="cards-block__card-title h3"><?php the_sub_field( 'hc_card_title' ); ?></h2>
                            <?php elseif( get_sub_field( 'hc_card_title' ) && !get_sub_field( 'hc_card_link' ) ) : ?>
                            <h3 class="cards-block__card-title"><?php the_sub_field( 'hc_card_title' ); ?></h3>
                            <?php endif; ?>
                            <?php if( get_sub_field( 'hc_card_intro' ) ) : ?>
                            <p class="cards-block__card-excerpt"><?php the_sub_field( 'hc_card_intro' ); ?></a></p>
                            <?php endif; ?>
                            </div>
                            <div class="cards-block__card-link text-center">
                                <?php if( get_sub_field( 'hc_card_link' ) && get_sub_field( 'hc_card_ex_link' ) ) : ?>
                                <a href="<?php echo get_sub_field( 'hc_card_link' )['url']; ?>" target="_blank" aria-label="Read more about <?php echo get_sub_field( 'hc_card_title' ); ?>"><?php echo get_sub_field( 'hc_card_link' )['title']; ?></a>
                                <?php elseif( get_sub_field( 'hc_card_link' ) && !get_sub_field( 'hc_card_ex_link' ) ) : ?>
                                <a href="<?php echo get_sub_field( 'hc_card_link' )['url']; ?>" aria-label="Read more about <?php echo get_sub_field( 'hc_card_title' ); ?>"><?php echo get_sub_field( 'hc_card_link' )['title']; ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php

/*
 * Logo Banner Block Template.
 */

?>
<?php if( have_rows( 'hc_logo_banner' ) ) : ?>
<div class="logo-banner block-margin">
    <div class="grid-container" data-equalizer data-equalizer-on="medium">
        <div class="grid-x grid-padding-x">
            <?php if( get_field( 'hc_logo_banner_title' ) ) : ?>
            <div class="cell small-12 text-center">
                <h2 class="logo-banner__title"><?php the_field( 'hc_logo_banner_title' ); ?></h2>
            </div>
            <?php endif; ?>
            <div class="cell small-12">
                <ul class="logo-banner__logos">
                <?php while ( have_rows( 'hc_logo_banner' ) ) : the_row(); ?>
                    <li>
                        <?php if( get_sub_field( 'hc_logo_banner_link' ) ) : ?>
                        <a href="<?php echo get_sub_field( 'hc_logo_banner_link' )['url']; ?>" target="_blank">
                        <?php endif; ?>
                            <?php echo wp_get_attachment_image( get_sub_field( 'hc_logo_banner_image' ), 'medium', true, 'class=logo-banner__image' ); ?>
                        <?php if( get_sub_field( 'hc_logo_banner_link' ) ) : ?>
                        <span class="sr-only"><?php echo get_sub_field( 'hc_logo_banner_link' )['title']; ?></span></a>
                        <?php endif; ?>
                    </li>
                <?php endwhile; ?>
                </ul>
            </div>
        </div>  
    </div>
</div>
<?php endif; ?>
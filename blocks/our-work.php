<?php

/**
 * Our work Block Template.
 **/

 ?>

<section id="client-logos" class="our-work-block block-margin">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div class="small-12 cell">
        <?php if( get_field( 'hex_our_work_sub_heading' ) ) : ?>
          <span class="our-work-block__subheading"><?php the_field( 'hex_our_work_sub_heading' ); ?></span>
        <?php endif; ?>
        <?php if( get_field( 'hex_our_work_heading' ) ) : ?>
          <h2 class="our-work-block__heading"><?php the_field( 'hex_our_work_heading' ); ?></h2>
        <?php endif; ?>
      </div>
    </div>
    <div class="grid-x grid-padding-x">
    <?php
      // Get 6 random ids of the 'ourwork' custom post type.
      $our_work_post_ids = get_posts(array(
          'fields'          => 'ids',
          'numberposts'     => 6,
          'orderby'         => 'rand',
          'post_type'       => 'ourwork'
      ));

      // Set the count at zero so we can use it to access the first item in the $our_work_post_ids array.
      $count = 0;

      // Get the total number of posts.
      $num_of_posts = count( $our_work_post_ids );
    ?>
    <?php while( $count <= ( $num_of_posts -1 ) ) : ?>
      <div class="small-6 medium-4 large-2 cell">
        <div class="our-work-block__client-logo">
          <a href="<?php echo get_permalink( $our_work_post_ids[$count] ); ?>" class="our-work-block__link">
            <?php echo wp_get_attachment_image( get_field( 'hex_our_work_client_logo', $our_work_post_ids[$count] ), 'small-square' ); ?>
            <?php echo get_the_title( $our_work_post_ids[$count] ); ?>
          </a>
        </div>
      </div>
      <?php $count++; ?>
    <?php endwhile; ?>
    </div>
  </div>
</section>
<?php

/**
 * Video Block Template.
 **/

?>

<div class="video-block b-margin">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell small-12 large-10 large-offset-1">
                <div class="video-block__inner responsive-embed">
                    <?php the_field( 'video_embed' ); ?> 
                </div>
            </div>
        </div>
    </div>
</div>
// Libraries
const chalk = require('chalk');
const log = require('fancy-log');
const fs = require('fs');
const path = require('path');

// Gulp + plugins
const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const size = require('gulp-size');
const noop = require('gulp-noop');
const replace = require('gulp-replace');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

// Setup Sass compiler
sass.compiler = require('node-sass');

// Check if we're running a production build (for minification, uglification, etc.)
const isProduction = (process.env.NODE_ENV === 'production');

// Compile Sass into CSS
function css() {
    return gulp.src([
        'src/scss/main.scss',
    ])
        .pipe(isProduction ? noop() : sourcemaps.init())
        .pipe(sass()).on('error', sass.logError)
        .pipe(!isProduction ? noop() : cleanCSS())
        .pipe(isProduction ? noop() : sourcemaps.write('.'))
        .pipe(gulp.dest('assets/css'))
        .pipe(size({
            showFiles: true,
            showTotal: false
        }));
}

// Compile JavaScript
function js() {
    return gulp.src([
        'src/js/app.js',
    ])
        .pipe(isProduction ? noop() : sourcemaps.init())
        .pipe(replace(/\/\/\s+@codekit-prepend\s+("(.*)"|'(.*)'|.*)/gmi, function (match, p1, p2) {
            const src = (p2 || p1);
            const filepath = path.resolve(path.dirname(this.file.path), src);
            if (fs.existsSync(filepath)) {
                log('Including \'' + chalk.green(path.basename(filepath)) + '\'...');
                return fs.readFileSync(filepath);
            } else {
                log('Unable to include \'' + chalk.red(src) + '\'');
                return match;
            }
        }))
        .pipe(!isProduction ? noop() : uglify())
        .pipe(isProduction ? noop() : sourcemaps.write('.'))
        .pipe(gulp.dest('assets/scripts'))
        .pipe(size({
            showFiles: true,
            showTotal: false
        }));
}

// Fonts
gulp.task('fonts', function() {
    return gulp.src([
        'node_modules/@fortawesome/fontawesome-pro/webfonts/**/*'])
        .pipe(gulp.dest('assets/fonts/fa'));
});

// Gulp tasks
exports.css = css;
exports.js = js;
exports.build = gulp.parallel(css, js);

// Gulp default task (watch for changes)
exports.default = function () {
    css();
    js();

    // Watch for changes to CSS & Sass files
    gulp.watch([
        'src/scss/**/*.css',
        'src/scss/**/*.scss',
    ], {
        usePolling: true
    }, css);

    // Watch for changes to JavaScript files
    gulp.watch([
        'src/js/*.js',
    ], {
        usePolling: true
    }, js);
}
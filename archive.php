<?php get_header(); ?>
<section id="main-content" role="main">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 cell">
				<h1><?php the_archive_title() ?></h1>
			</div>
		</div>
	</div>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 medium-6 large-4 cell">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'templates/content', 'archive' ); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_template_part( 'templates/section', 'pagination' ); ?>
<?php get_footer(); ?>

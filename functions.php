<?php

/**
 * 	Main files
 */

include('functions/tidy-wordpress.php');
include('functions/accessibility.php');
// include('functions/custom-posts.php');
include('functions/advanced-custom-fields.php');
include('functions/acf-blocks.php');
include('functions/shortcodes.php');
// include('functions/menu.php');
// include('functions/image-gallery.php');
include('functions/classic-editor/main.php');
include('functions/video-title/main.php');
include('functions/hex-admin.php');
include('functions/gutenberg.php');

/**
 * 	Scripts & Styles
 */

 function scriptsandstyle() {
 	wp_dequeue_style( 'wp-block-library-css' );
 	wp_deregister_script('wp-embed');
 	// wp_deregister_script('jquery');
 	wp_enqueue_style( 'main-style', get_template_directory_uri() . '/assets/css/main.css', false, filemtime(get_parent_theme_file_path() . '/assets/css/main.css') );
 	wp_enqueue_script( 'application', get_template_directory_uri() . '/assets/scripts/app.js', array(), filemtime(get_parent_theme_file_path() . '/assets/scripts/app.js'), true );
 }
 add_action( 'wp_enqueue_scripts', 'scriptsandstyle', 999);

/**
 * 	Help with setup
 */

function add_excerpt_support(){
   add_post_type_support( 'post', 'excerpt' );
	 add_post_type_support( 'page', 'excerpt' );
}
add_action('admin_init', 'add_excerpt_support');

add_theme_support( 'post-thumbnails' );

/**
 * 	Menus
 */

register_nav_menu( 'Header', 'main_menu' );
register_nav_menu( 'Footer', 'footer_menu' );

/**
 * 	Featured image sizes
 */

set_post_thumbnail_size( 640, 360, true );

add_image_size( 'square', 640, 640, true );
add_image_size( 'small-square', 320, 320, true );
add_image_size( 'small-landscape', 640, 360, true );
add_image_size( 'landscape', 970, 546, true );
add_image_size( 'portrait', 600, 840, true );
add_image_size( 'hero-banner', 1600, 400, true );
add_image_size( 'snippet', 590, 330, true );

/**
 * 	Add slug to body class
 */

function add_slug_body_class( $classes ) {
	global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_type . '-' . $post->post_name;
		}
	return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );

/**
 * 	Format phone number
 */

function format_phone( $string ) {
	return '<a href="tel:+44' . preg_replace("/[^A-Za-z0-9]/", "", $string) . '" class="phone-number">' . $string . '</a>';
}

/**
* 	Footer widgets
*/

add_action( 'widgets_init', 'hex_footer_widget' );

function hex_footer_widget() {
   register_sidebar( array(
       'name' => __( 'Footer Widgets', 'hex-theme' ),
       'id' => 'footer-widgets',
       'description' => __( 'Widget in this area will be shown in the footer.', 'hex-theme' ),
       'before_widget' => '<div class="small-12 medium-6 large-3 cell"><div class="footer-widget">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
   ) );
}

/**
*   Move Yoast SEO box to bottom of the editor screen.
*/

function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom' );

/**
 * 	ACF Licence
 */

class AutoActivator {

 		const ACTIVATION_KEY = 'b3JkZXJfaWQ9NjE2NDh8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE1LTA4LTExIDE0OjQ1OjIz';

 		/**
 		 * AutoActivator constructor.
 		 * This will update the license field option on acf
 		 * Works only on backend to not attack performance on frontend
 		 */
 		public function __construct() {
 			if (
 				function_exists( 'acf' ) &&
 			    is_admin() &&
 				!acf_pro_get_license_key()
 			) {
 				acf_pro_update_license(self::ACTIVATION_KEY);
 			}
 		}

 	}

new AutoActivator();

/**
* 	Style login page
*/

function hex_login_style() { ?>
	<style type="text/css">
		body.login {
			background: #243886;
		}

		body.login div#login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logos/);
			background-size: contain;
			padding: 2rem 0 0;
			width: 75%;
		}

		body.login .message {
			border-left: 4px solid #175FA5;
		}

		body.login .button-primary {
			background: #243886;
			border-color: #243886;
		}

		body.login .button-primary:active,
		body.login .button-primary:focus,
		body.login .button-primary:hover {
			background: #243886;
			border-color: #243886;
		}

		body.login .privacy-policy-link,
		body.login #nav a,
		body.login #backtoblog a {
			color: #FFFFFF;
			font-weight: bold;
		}

		body.login .privacy-policy-link:hover,
		body.login #nav a:hover,
		body.login #backtoblog a:hover {
			color: #FFFFFF;
			text-decoration: underline;
		}

		#wp-auth-check-wrap #wp-auth-check {
			background-color: #243886;
		}
	</style>
<?php }

add_action( 'login_enqueue_scripts', 'hex_login_style' );

?>

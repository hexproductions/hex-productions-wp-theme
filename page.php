<?php get_header(); ?>
<section id="main-content" role="main">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 cell" id="post-<?php the_ID(); ?>">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'templates/content', 'page' ); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
	<?php the_content(); ?>
</section>
<?php get_footer(); ?>

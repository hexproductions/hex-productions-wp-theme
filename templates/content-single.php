<h1><?php the_title(); ?></h1>
<?php if( has_excerpt() ) : ?>
<div class="intro">
	<?php the_excerpt(); ?>
</div>
<?php endif; ?>


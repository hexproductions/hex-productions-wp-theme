<?php if( has_post_thumbnail() ) : ?>
<?php $image_url = get_the_post_thumbnail_url( null, 'hero-banner' ); ?>
<div class="hero-banner hero-banner--image" style="background-image:url('<?php echo $image_url; ?>')">
<?php else : ?>
<div class="hero-banner hero-banner--image" style="background-image:url('<?php echo get_template_directory_uri() . '/assets/img/hero-banner.jpg'; ?>')">
<?php endif; ?>
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="small-10 small-offset-1 medium-8 medium-offset-0 large-6 cell">
                <div class="hero-banner__content">
                    <h1 class="hero-banner__title"><?php the_title(); ?></h1>
                    <?php if( has_excerpt() ) : ?>
                    <p class="hero-banner__excerpt"><?php echo get_the_excerpt(); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

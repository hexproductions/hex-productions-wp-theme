<div class="grid-container block-margin">
	<div class="grid-x grid-padding-x">
		<div class="cell small-12">
			<?php
			if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb( '<nav id="breadcrumbs" aria-label="breadcrumbs">','</nav>' );
			}
			?>
		</div>
	</div>
</div>
<?php if( get_field( 'alert_message', 'option' ) && get_field( 'alert_checkbox', 'option' ) && is_front_page() ) : ?>
<div class="site-alert hide">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
		<div class="small-12 cell text-center">
				<div class="site-alert__content">
					<p class="site-alert__message">
						<i class="fa fa-exclamation-triangle" aria-hidden="true"></i><?php the_field( 'alert_message', 'option' ); ?>
						<?php $link = get_field( 'alert_link', 'option' );
							if( $link ) :
							$link_url = $link['url'];
							$link_title = $link['title'];	
						?>
						<a href="<?php echo $link_url ?>"><?php echo $link_title; ?></a>
						<?php endif; ?>
						<button class="site-alert__close" aria-label="Dismiss alert" type="button">
							<i class="far fa-times-circle"></i>
						</button>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php elseif( get_field( 'alert_message', 'option' ) && !get_field( 'alert_checkbox', 'option' ) ) : ?>
<div class="site-alert hide">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 cell text-center">
				<div class="site-alert__content">
					<p class="site-alert__message">
						<i class="fa fa-exclamation-triangle" aria-hidden="true"></i><?php the_field( 'alert_message', 'option' ); ?>
						<?php $link = get_field( 'alert_link', 'option' );
							if( $link ) :
							$link_url = $link['url'];
							$link_title = $link['title'];	
						?>
						<a href="<?php echo $link_url ?>"><?php echo $link_title; ?></a>
						<?php endif; ?>
						<button class="site-alert__close" aria-label="Dismiss alert" type="button">
							<i class="far fa-times-circle"></i>
						</button>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php if( has_post_thumbnail() ) : ?>
<?php echo get_the_post_thumbnail( get_the_ID(), 'hero-banner', 'class=featured-image block-margin' ); ?>
<?php endif; ?>
<h1><?php the_title(); ?></h1>
<?php if( has_excerpt() ) : ?>
<div class="intro">
	<?php the_excerpt(); ?>
</div>
<?php endif; ?>

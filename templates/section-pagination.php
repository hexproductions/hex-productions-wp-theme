<?php
/**
 * Create numeric pagination in WordPress
 */

global $wp_query;
$total = $wp_query->max_num_pages;
if ( $total > 1 )  {

?>
<section id="pagination">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 medium-10 medium-offset-1 text-center cell">
				<nav aria-label="Pagination">
				<?php
					if ( !$current_page = get_query_var( 'paged' ) )
					$current_page = 1;
					$format = empty( get_option( 'permalink_structure' ) ) ? '&page=%#%' : 'page/%#%/';
					echo paginate_links( array(
						'base' 		=> preg_replace('/\?.*/', '', get_pagenum_link(1)) . '%_%',
						'format' 	=> $format,
						'current' 	=> $current_page,
						'total' 	=> $total,
						'mid_size' 	=> 1,
						'type' 		=> 'list'
					) );
				?>
				</nav>
			</div>
		</div>
	</div>
</section>
<?php } ?>

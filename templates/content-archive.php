<div class="post-snippet" id="post-<?php the_ID(); ?>">
	<div class="post-thumbnail">
		<?php the_post_thumbnail( 'landscape' ); ?>
	</div>
	<div class="post-content">
		<h2><?php the_title(); ?></h2>
		<?php if( has_excerpt() ) : ?>
		<div class="post-excerpt">
			<?php the_excerpt(); ?>
		</div>
		<?php endif; ?>
		<p>
			<a href="<?php the_permalink(); ?>" class="button"><?php _e( 'Read more about ', 'hex-theme' ); ?><?php the_title(); ?></a>
		</p>
	</div>
</div>

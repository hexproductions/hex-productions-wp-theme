# HeX Productions WordPress Theme

_A base theme for Wordpress based on Foundation 6._

## Developer Setup

### Requirements

* __[Node.js](https://nodejs.org/)__ (version 12.0 or later)

### Useful tip

You may find it better in the long term to install `gulp` globally. To do this, run:

`npm install --global gulp-cli`

If you run into permission errors, please try the following:

`sudo npm install --global gulp-cli`

Full documentation is available at [gulpjs.com](https://gulpjs.com/).

### Installation

From within the theme's directory (e.g. `hex-productions-wp-theme`), open a new terminal window (or command prompt) and run the following command:

`npm install`

### How to compile assets

From within the theme's directory (e.g. `hex-productions-wp-theme`), open a new terminal window (or command prompt) and follow the instructions for your preferred build from the options below.

#### Development build

If you have `gulp` installed globally, run:

`gulp build`

Otherwise, try:

`npx gulp build`

Source maps will be generated for a development-quality build.

#### Production build

If you have `gulp` installed globally, run:

`NODE_ENV=production gulp build`

Alternatively, try:

`NODE_ENV=production npx gulp build`

__Important:__ Source maps are not generated for a production-quality build, as these files can become very large and inefficient on a production server.

### Compiling assets automatically during development

Building assets and watching files for changes can be done automatically.

If you have `gulp` installed globally, run:

`gulp`

If not, try:

`npx gulp`

When this command is run, the assets will be compiled (development-quality) and source maps will be generated for debugging purposes.

### Notes

* __Development-quality__ means _fast_, _non-optimised_ builds of style sheets and JavaScript files.

* __Production-quality__ means _optimised_ builds of style sheets and JavaScript files. This is not recommended during development as these builds take considerably longer to perform.

### Troubleshooting

#### Issues with node-sass

If you run into an error regarding support for your current environment, please try running the following command:

`npm rebuild node-sass`

_Please note that you may need to install Python 2.7 and a C++ compiler (such as GNU C++) to accomplish this._

Alternatively, try deleting (or renaming) your `node_modules` directory and running `npm install` again.
